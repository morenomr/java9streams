package com.netsuite.suitetalks.model;

public class EmployeeBuilder
{
	private int age;
	private String name;

	EmployeeBuilder()
	{

	}

	public static EmployeeBuilder anEmployee()
	{
		return new EmployeeBuilder();
	}

	public EmployeeBuilder withAge(int age)
	{
		this.age = age;
		return this;
	}

	public EmployeeBuilder withName(String name)
	{
		this.name = name;
		return this;
	}

	public Employee build()
	{
		return new Employee(age, name);
	}
}