package com.netsuite.suitetalks;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

public class MyBenchmark
{

	public static void main(String[] args) throws Exception
	{
		Options opt = new OptionsBuilder()
				.include(".*" + MyBenchmark.class.getSimpleName() + ".*")
				.warmupIterations(3)
				.measurementIterations(10)
				.build();
		new Runner(opt).run();
	}

	@Benchmark
	@BenchmarkMode(Mode.SingleShotTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public Object noStreams(BenchmarkState state)
	{
		return new Object();
	}

	@Benchmark
	@BenchmarkMode(Mode.SingleShotTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public Object java8(BenchmarkState state)
	{
		return new Object();
	}

	@Benchmark
	@BenchmarkMode(Mode.SingleShotTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public Object java9(BenchmarkState state)
	{
		return new Object();
	}

	@State(Scope.Benchmark)
	public static class BenchmarkState
	{
	}
}
